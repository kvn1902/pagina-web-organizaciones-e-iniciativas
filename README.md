# Django Web App: Organizaciones e iniciativas

*** Project development in progress *** 

This is a web app meant to promote public and private organizations, as well as iniciatives present in the Osa Peninsula, Costa Rica. 

## Requirements

It is recommended to use a virtual environment:

```
python3 -m venv <environment_name>
```

Activate the virtual environment:

```
source <environment_name>/bin/activate
```

Install the requirements:

```
pip install -r requirements.txt
```

## Run the code

Go to organizaciones_iniciativas/ and run the following command:

```
python manage.py runserver
```
