// newItemName = JSON.parse(document.currentScript.getAttribute("lista"));

var elementosFiltro = JSON.parse(document.currentScript.getAttribute('elementosFiltro'));
var listaDeObjetos = document.getElementsByClassName("objeto");
var checkboxes = document.getElementsByClassName("checkboxFiltro");
var checkboxesSeleccionados = [];
var index, elemento;

for (let i=0; i<checkboxes.length; i++){
    checkboxes[i].addEventListener("click", (event) => {
        
        // Agregar a la lista
        if (checkboxesSeleccionados.includes(checkboxes[i].id) == false) {
            checkboxesSeleccionados.push(checkboxes[i].id);
        }
        // Eliminar de la lista
        else{
            index = checkboxesSeleccionados.indexOf(checkboxes[i].id);
            if (index > -1) {
                checkboxesSeleccionados.splice(index, 1);
            }
        }

        // Filtro
        for (let j=0; j<elementosFiltro.length; j++){
            elemento = document.getElementById("objeto".concat(elementosFiltro[j][0]));
            elemento.style.display = "none";
            for (let i=0; i< checkboxesSeleccionados.length; i++){
                if(elementosFiltro[j][1].includes(parseInt(checkboxesSeleccionados[i].replace('etiqueta','')))){
                    elemento.style.display = "";
                }
            }
        }

        // Cuando no hay cheboxes seleccionados, se muestra todo
        if (checkboxesSeleccionados.length==0){
            for (let j=0; j<elementosFiltro.length; j++){
                elemento = document.getElementById("objeto".concat(elementosFiltro[j][0]));
                elemento.style.display = "";
            }
        }

    });
}