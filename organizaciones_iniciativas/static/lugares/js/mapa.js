const tilesProvider = "https://tile.openstreetmap.org/{z}/{x}/{y}.png"

let myMap = L.map('myMap').setView([8.6257781,-83.5017215], 11)

L.tileLayer(tilesProvider, {
    maxZoom: 18
}).addTo(myMap)

var ubicaciones = JSON.parse(document.currentScript.getAttribute('ubicaciones'));
console.log(ubicaciones)

// let marker = L.marker([9.602229,-84.1984192]).addTo(myMap)

for (let i=0; i<ubicaciones.length; i++){
        let marker = L.marker([ubicaciones[i][1][0],ubicaciones[i][1][1]])
                        .addTo(myMap)
                        .bindPopup(ubicaciones[i][0]);
}