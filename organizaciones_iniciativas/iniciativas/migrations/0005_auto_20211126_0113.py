# Generated by Django 3.2.9 on 2021-11-26 01:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('etiquetas', '0001_initial'),
        ('lugares', '0002_auto_20211126_0050'),
        ('iniciativas', '0004_iniciativa_codigo'),
    ]

    operations = [
        migrations.AddField(
            model_name='iniciativa',
            name='etiquetas',
            field=models.ManyToManyField(to='etiquetas.Etiqueta'),
        ),
        migrations.AddField(
            model_name='iniciativa',
            name='lugares',
            field=models.ManyToManyField(to='lugares.Lugar'),
        ),
    ]
