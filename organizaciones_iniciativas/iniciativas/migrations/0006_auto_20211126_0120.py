# Generated by Django 3.2.9 on 2021-11-26 01:20

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('iniciativas', '0005_auto_20211126_0113'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='fuentefinanciamiento',
            options={'verbose_name': 'Fuente de financiamiento', 'verbose_name_plural': 'Fuentes de financiamiento'},
        ),
        migrations.AlterModelOptions(
            name='poblacionmeta',
            options={'verbose_name': 'Poblacion meta', 'verbose_name_plural': 'Poblaciones meta'},
        ),
    ]
