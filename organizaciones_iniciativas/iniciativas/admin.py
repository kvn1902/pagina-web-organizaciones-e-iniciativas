from django.contrib import admin
from .models import *

admin.site.register(Iniciativa)
admin.site.register(PoblacionMeta)
admin.site.register(FuenteFinanciamiento)
admin.site.register(TipoIniciativa)
