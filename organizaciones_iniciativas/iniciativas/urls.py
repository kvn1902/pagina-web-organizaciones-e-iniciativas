from django.urls import path
from . import views

urlpatterns = [
    path('', views.iniciativas),
    path('<str:iniciativa>/', views.iniciativa),
]