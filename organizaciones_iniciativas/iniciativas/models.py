from django.db import models
import datetime as dt
import os


def path_file_name(instance, filename):
    return os.path.join('iniciativas', instance.nombre, filename)


class Iniciativa(models.Model): 
    nombre = models.CharField(max_length=200)
    nombre_corto = models.CharField(max_length=200)
    descripcion_corta = models.CharField(max_length=1000)
    descripcion = models.TextField()
    objetivo = models.CharField(max_length=500)
    objetivos = models.TextField()
    responsable = models.ForeignKey('entes.Ente', on_delete=models.CASCADE)           
    colaboradores = models.ManyToManyField('entes.Ente', blank=True, 
                                related_name='ColaboraEn')
    vigencia_i = models.DateField()
    vigencia_f = models.DateField()
    lugares = models.ManyToManyField('lugares.Lugar')           
    articulaciones = models.ManyToManyField('self', blank=True)                  
    enlace = models.CharField(max_length=1000)
    piosa = models.BooleanField()
    etiquetas = models.ManyToManyField('etiquetas.Etiqueta')
    eventos = models.ManyToManyField('eventos.Evento', blank=True)
    poblacion_meta = models.ManyToManyField('PoblacionMeta')
    fuente_de_financiamiento = models.ManyToManyField('FuenteFinanciamiento')
    principales_resultados = models.TextField()
    tipo = models.ForeignKey('TipoIniciativa', blank=True, null=True, 
                                    on_delete=models.SET_NULL)
    codigo = models.CharField(max_length=100, blank=True, null=True)
    imagen = models.ImageField(upload_to=path_file_name)

    def __str__(self):
        return self.nombre 

    def vigente(self):
        if (dt.date.today() >= self.vigencia_i and 
            dt.date.today() <= self.vigencia_f):
            return True 
        else:
            return False

    def obtener_iniciativas_vigentes(self):
        iniciativas_vigentes = []
        for iniciativa in Iniciativa.objects.all():
            if iniciativa.vigente:
                iniciativas_vigentes.append(iniciativa)
        return iniciativas_vigentes


class PoblacionMeta(models.Model):
    nombre = models.CharField(max_length=200)
    descripcion = models.TextField()

    def __str__(self):
        return self.nombre 

    class Meta:
        verbose_name = "Poblacion meta"
        verbose_name_plural = "Poblaciones meta"


class FuenteFinanciamiento(models.Model):
    nombre = models.CharField(max_length=200)
    descripcion = models.TextField()
    ente = models.ManyToManyField('entes.Ente')

    def __str__(self):
        return self.nombre 

    class Meta:
        verbose_name = "Fuente de financiamiento"
        verbose_name_plural = "Fuentes de financiamiento"


class TipoIniciativa(models.Model):
    nombre = models.CharField(max_length=200)
    descripcion = models.TextField()

    def __str__(self):
        return self.nombre 

