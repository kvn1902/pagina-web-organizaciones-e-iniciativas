from django.shortcuts import render
from .models import Iniciativa
from etiquetas.models import Etiqueta
from entes.models import Organizacion
from webpage.models import Textos

def iniciativas(request):
    
    iniciativas = Iniciativa.objects.order_by('?')
    elementosFiltro = []

    for iniciativa in iniciativas:        
        etiquetas = []
        for etiqueta in iniciativa.etiquetas.all():
            etiquetas.append(etiqueta.id)
        iniciativaYEtiquetas = [iniciativa.id, etiquetas]
        elementosFiltro.append(iniciativaYEtiquetas)
    
    context = {
        "lista": iniciativas,
        "textos": Textos.objects.all()[0],
        "etiquetas": Etiqueta.objects.all(),
        "elementosFiltro": elementosFiltro  
    }
    return render(request, "iniciativas/iniciativas.html", context)


def iniciativa(request, iniciativa):
    try: 
        iniciativa = Iniciativa.objects.get(nombre=iniciativa)
        contexto = {
            "iniciativa": iniciativa
        }
        return render(request, "iniciativas/iniciativa.html", contexto)    
    except:
        return render(request, "webpage/noexiste.html")