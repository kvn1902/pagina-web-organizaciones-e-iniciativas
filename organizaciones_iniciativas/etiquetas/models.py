from django.db import models

class Etiqueta(models.Model):
    nombre = models.CharField(max_length=150)
    descripcion = models.TextField()
    categoria = models.ForeignKey('CategoriaEtiqueta', blank=True, null=True,
                                        on_delete=models.SET_NULL)

    def __str__(self):
        return self.nombre

    def obtener_valor(self):
        return self.nombre.replace(" ", "-")


class CategoriaEtiqueta(models.Model):
    nombre = models.CharField(max_length=150)
    descripcion = models.TextField()

    def __str__(self):
        return self.nombre
