from django.shortcuts import render
from .models import Evento
from etiquetas.models import Etiqueta
from webpage.models import Textos


def eventos(request):
    contexto = {
        "eventos": Evento.objects.order_by('inicio'),
        "etiquetas": Etiqueta.objects.all(),
        "textos": Textos.objects.all()[0]
    }

    return render(request, "eventos/eventos.html", contexto)


def evento(request, evento):
    try: 
        evento = Evento.objects.get(nombre=evento)
        contexto = {
            "item": evento
        }
        return render(request, "eventos/evento.html", contexto)    
    except:
        return render(request, "webpage/noexiste.html")