from django.urls import path
from . import views

urlpatterns = [
    path('', views.eventos),
    path('<str:evento>/', views.evento),
]