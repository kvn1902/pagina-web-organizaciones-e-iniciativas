from django.db import models
import os

meses = ["enero", "febrero","marzo","abril","mayo","junio","julio", \
        "agosto", "setiembre","octubre","noviembre","diciembre"] 


def path_file_name(instance, filename):
    return os.path.join('eventos', instance.nombre, filename)


class Evento(models.Model):
    nombre = models.CharField(max_length=200)
    descripcion_corta = models.TextField(max_length=500)
    descripcion = models.TextField()
    lugar = models.ForeignKey('lugares.Lugar', null=True, blank=True,
                                on_delete=models.SET_NULL)
    enlace = models.CharField(max_length=200, null=True, blank=True)
    inicio = models.DateField()
    final = models.DateField()
    etiquetas = models.ManyToManyField('etiquetas.Etiqueta')
    imagen = models.ImageField(upload_to=path_file_name)

    def __str__(self):
        return self.nombre 

    def obtener_inicio(self):
        fechaInicio = str(self.inicio).split("-") 
        mes = str(meses[int(fechaInicio[1]) - 1])
        dia = fechaInicio[2] 
        ano = fechaInicio[0] 
        return str( dia + " de " + mes + ", " + ano )

    def obtener_final(self):
        fechafin = str(self.final).split("-") 
        mes = str(meses[int(fechafin[1]) - 1])
        dia = fechafin[2] 
        ano = fechafin[0] 
        return str( dia + " de " + mes + ", " + ano )


