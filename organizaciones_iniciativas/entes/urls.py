from django.urls import path
from . import views

urlpatterns = [
    path('organizaciones/', views.organizaciones),
    path('organizaciones/<str:organizacion>/', views.organizacion),
    path('personas/', views.personas),
    path('personas/<str:persona>/', views.persona),
]