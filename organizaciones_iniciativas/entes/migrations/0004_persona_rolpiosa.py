# Generated by Django 3.2.9 on 2021-11-18 20:10

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('entes', '0003_auto_20211118_2002'),
    ]

    operations = [
        migrations.CreateModel(
            name='RolPiosa',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='Persona',
            fields=[
                ('ente_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='entes.ente')),
                ('cedula_fisica', models.CharField(max_length=30)),
                ('lider_comunal', models.BooleanField()),
                ('rol_piosa', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='entes.rolpiosa')),
            ],
            bases=('entes.ente',),
        ),
    ]
