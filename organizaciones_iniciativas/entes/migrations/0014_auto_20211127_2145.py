# Generated by Django 3.2.9 on 2021-11-27 21:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('entes', '0013_alter_organizacion_imagen'),
    ]

    operations = [
        migrations.AddField(
            model_name='ente',
            name='descripcion_corta',
            field=models.TextField(default=1, max_length=1000),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='ente',
            name='descripcion',
            field=models.TextField(),
        ),
    ]
