# Generated by Django 3.2.9 on 2021-11-18 20:02

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('entes', '0002_auto_20211118_1953'),
    ]

    operations = [
        migrations.AddField(
            model_name='ente',
            name='cel_publico',
            field=models.BooleanField(default=1),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='ente',
            name='correo_publico',
            field=models.BooleanField(default=1),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='ente',
            name='tel_publico',
            field=models.BooleanField(default=1),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='ente',
            name='universitario',
            field=models.BooleanField(default=1),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='ente',
            name='superior_jerarquico',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='entes.ente'),
        ),
    ]
