# Generated by Django 3.2.9 on 2021-11-18 20:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('entes', '0005_rolpiosa_descripcion'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='persona',
            name='rol_piosa',
        ),
        migrations.AddField(
            model_name='persona',
            name='rol_piosa',
            field=models.ManyToManyField(to='entes.RolPiosa'),
        ),
    ]
