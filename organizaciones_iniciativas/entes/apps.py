from django.apps import AppConfig


class EntesConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'entes'
