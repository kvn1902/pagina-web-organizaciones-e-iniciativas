from django.shortcuts import render
from .models import Organizacion, Persona
from webpage.models import Textos
from etiquetas.models import Etiqueta

def organizaciones(request):

    organizaciones = Organizacion.objects.order_by('?')
    elementosFiltro = []

    for organizacion in organizaciones:        
        etiquetas = []
        for etiqueta in organizacion.etiquetas.all():
            etiquetas.append(etiqueta.id)
        organizacionYEtiquetas = [organizacion.id, etiquetas]
        elementosFiltro.append(organizacionYEtiquetas)

    context = {
        "lista": organizaciones,
        "textos": Textos.objects.all()[0],
        "etiquetas": Etiqueta.objects.all(),
        "active": [1, 0, 0],
        "elementosFiltro": elementosFiltro
    }

    return render(request, "entes/organizaciones.html", context)


def organizacion(request, organizacion):
    try: 
        organizacion = Organizacion.objects.get(nombre=organizacion)
        contexto = {
            "organizacion": organizacion
        }
        return render(request, "entes/organizacion.html", contexto)    
    except:
        return render(request, "webpage/noexiste.html")


def personas(request):

    personas = Persona.objects.order_by('?').filter(es_publica=True)
    elementosFiltro = []

    for persona in personas:        
        etiquetas = []
        for etiqueta in persona.etiquetas.all():
            etiquetas.append(etiqueta.id)
        personaYEtiquetas = [persona.id, etiquetas]
        elementosFiltro.append(personaYEtiquetas)

    context = {
        "lista": personas,
        "textos": Textos.objects.all()[0],
        "etiquetas": Etiqueta.objects.all(),
        "active": [1, 0, 0],
        "elementosFiltro": elementosFiltro
    }
    return render(request, "entes/personas.html", context)


def persona(request, persona):
    try: 
        persona = Persona.objects.get(nombre=persona)
        contexto = {
            "persona": persona
        }
        return render(request, "entes/persona.html", contexto)    
    except:
        return render(request, "webpage/noexiste.html")