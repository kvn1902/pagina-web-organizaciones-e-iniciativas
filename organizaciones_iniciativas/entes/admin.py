from django.contrib import admin
from .models import *

admin.site.register(Ente)
admin.site.register(Persona)
admin.site.register(RolPiosa)
admin.site.register(Organizacion)
admin.site.register(TipoOrganizacion)
