from django.db import models
import os


CHOICES = (
    ("Publica", "Publica"),
    ("Privada", "Privada")
)


def path_file_name(instance, filename):
    return os.path.join('organizaciones', instance.nombre, filename)


class Ente(models.Model):
    nombre = models.CharField(max_length=100)
    nombre_corto = models.CharField(max_length=100)
    descripcion_corta = models.TextField(max_length=1000)
    descripcion = models.TextField()    
    correo = models.CharField(max_length=50)
    correo_publico = models.BooleanField()
    tel = models.CharField(max_length=15)
    tel_publico = models.BooleanField()
    cel = models.CharField(max_length=15)
    cel_publico = models.BooleanField()
    enlace = models.CharField(max_length=500)
    etiquetas = models.ManyToManyField('etiquetas.Etiqueta')
    universitario = models.BooleanField()
    lugares = models.ManyToManyField('lugares.Lugar', blank=True)
    superior_jerarquico = models.ForeignKey('self', blank=True, null=True, 
                            on_delete=models.SET_NULL)

    def __str__(self):
        return self.nombre

    def obtener_valor(self):
        return self.nombre.replace(" ", "-")


class Persona(Ente):
    cedula_fisica = models.CharField(max_length=30)
    lider_comunal = models.BooleanField()
    rol_piosa = models.ManyToManyField('RolPiosa', blank=True)
    es_publica = models.BooleanField()
    imagen = models.ImageField(upload_to=path_file_name)


class RolPiosa(models.Model):
    nombre = models.CharField(max_length=50)
    descripcion = models.TextField(max_length=1000)

    def __str__(self):
        return self.nombre


class Organizacion(Ente):
    representante = models.ForeignKey('Persona', blank=True, null=True, 
                                     on_delete=models.SET_NULL, related_name='Representa')
    persona_de_contacto = models.ForeignKey('Persona', blank=True, null=True, 
                                     on_delete=models.SET_NULL, related_name='ContactoDe')
    miembros = models.ManyToManyField('Persona')
    cedula_juridica = models.CharField(max_length=20)
    tipo = models.ForeignKey('TipoOrganizacion', blank=True, null=True, 
                                     on_delete=models.SET_NULL)
    imagen = models.ImageField(upload_to=path_file_name)


class TipoOrganizacion(models.Model):
    nombre = models.CharField(max_length=30) 
    tipo = models.CharField(max_length=10, choices=CHOICES)

    def __str__(self):
        return self.nombre
