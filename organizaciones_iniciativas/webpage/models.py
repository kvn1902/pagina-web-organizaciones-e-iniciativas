from django.db import models


class Textos(models.Model):    
    titulo = models.CharField(max_length=200)
    acercaDe = models.TextField()

    def __str__(self):
        return self.titulo


class PieDePagina(models.Model):    
    titulo = models.CharField(max_length=200)
    email = models.CharField(max_length=200, blank=True, null=True)
    tel = models.CharField(max_length=200, blank=True, null=True)
    facebook = models.CharField(max_length=200, blank=True, null=True)
    youtube = models.CharField(max_length=200, blank=True, null=True)
    instagram = models.CharField(max_length=200, blank=True, null=True)

    def __str__(self):
        return self.titulo

