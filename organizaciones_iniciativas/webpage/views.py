from django.shortcuts import render
from entes.models import Ente, Organizacion
from iniciativas.models import Iniciativa, PoblacionMeta
from eventos.models import Evento
from etiquetas.models import Etiqueta
from .models import Textos
import json, random

def index(request):

    # Etiquetas, lugares y poblaciones meta 
    # de entes, iniciativas y eventos
    todosLosElementos = list()
    elementosNube = list()
    cantidad_elementos = list()

    entes = list(Ente.objects.all())
    iniciativas = list(Iniciativa.objects.all())
    organizaciones = list(Organizacion.objects.all())
    eventos = list(Evento.objects.all())

    for ente in entes:
        for etiqueta in ente.etiquetas.all():
            todosLosElementos.append(etiqueta.nombre)
        for lugar in ente.lugares.all():
            if lugar.region:
                todosLosElementos.append(lugar.region.nombre)

    for iniciativa in iniciativas:
        for etiqueta in iniciativa.etiquetas.all():
            todosLosElementos.append(etiqueta.nombre)
        for poblacionMeta in iniciativa.poblacion_meta.all():
            todosLosElementos.append(poblacionMeta.nombre)
        for lugar in iniciativa.lugares.all():
            if lugar.region:
                todosLosElementos.append(lugar.region.nombre)

    for evento in eventos:
        for etiqueta in evento.etiquetas.all():
            todosLosElementos.append(etiqueta.nombre)
        if evento.lugar.region:
            todosLosElementos.append(evento.lugar.region.nombre)

    for elemento in todosLosElementos:
        if elemento not in elementosNube:
            elementosNube.append(elemento)            
            cantidades = todosLosElementos.count(elemento)
            cantidad_elementos.append(cantidades)
    
    random.shuffle(organizaciones)
    random.shuffle(iniciativas)

    context = {
        "lista_organizaciones": organizaciones,
        "lista_iniciativas": iniciativas,
        "textos": Textos.objects.all()[0],
        "elementosNube": json.dumps(elementosNube),
        "cantidad_elementos": cantidad_elementos,
        "active": [1, 0, 0]

    }
    return render(request, "webpage/index.html", context)


def acercaDe(request):

    contexto = {        
        "textos": Textos.objects.all()[0]
    }

    return render(request, "webpage/acercaDe.html", contexto)
