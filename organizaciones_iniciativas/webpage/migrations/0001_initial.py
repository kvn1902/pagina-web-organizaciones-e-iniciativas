# Generated by Django 3.2.9 on 2021-11-26 00:56

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Footer',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('titulo', models.CharField(max_length=200)),
                ('email', models.CharField(max_length=200)),
                ('tel', models.CharField(max_length=200)),
                ('facebook', models.CharField(max_length=200)),
                ('youtube', models.CharField(max_length=200)),
                ('instagram', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='Textos',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('titulo', models.CharField(max_length=200)),
            ],
        ),
    ]
