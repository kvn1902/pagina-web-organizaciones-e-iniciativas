from django.db import models


class Lugar(models.Model):
    nombre = models.CharField(max_length=200)
    region = models.ForeignKey('Region', blank=True, null=True,
                                on_delete=models.SET_NULL)
    provincia = models.ForeignKey('Provincia', blank=True, null=True,
                                on_delete=models.SET_NULL)
    canton = models.ForeignKey('Canton', blank=True, null=True,
                                on_delete=models.SET_NULL)
    distrito = models.ForeignKey('Distrito', blank=True, null=True,
                                on_delete=models.SET_NULL)
    coordenadas = models.CharField(max_length=1000)
    direccion = models.TextField()

    def __str__(self):
        return self.nombre

    def obtener_latitud(self):        
        return float(self.coordenadas.split(",")[0])

    def obtener_longitud(self):        
        return float(self.coordenadas.split(",")[1])


class Region(models.Model):
    nombre = models.CharField(max_length=200)
    coordenadas = models.CharField(max_length=1000)

    def __str__(self):
        return self.nombre


class Provincia(models.Model):
    nombre = models.CharField(max_length=200)
    coordenadas = models.CharField(max_length=1000)

    def __str__(self):
        return self.nombre


class Canton(models.Model):
    nombre = models.CharField(max_length=200)
    coordenadas = models.CharField(max_length=1000)

    def __str__(self):
        return self.nombre


class Distrito(models.Model):
    nombre = models.CharField(max_length=200)
    coordenadas = models.CharField(max_length=1000)

    def __str__(self):
        return self.nombre
