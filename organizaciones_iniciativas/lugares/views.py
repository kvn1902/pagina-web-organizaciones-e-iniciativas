from django.shortcuts import render
from .models import Lugar
from webpage.models import Textos
from json import dumps

def mapa(request):

    ubicaciones = []
    lugares = Lugar.objects.all()

    for lugar in lugares: 
        coordenadas = [lugar.obtener_latitud(), lugar.obtener_longitud()]
        ubicaciones.append([lugar.nombre, coordenadas])

    context = {
        "ubicaciones": dumps(ubicaciones),
        "textos": Textos.objects.all()[0]
    }

    return render(request, "lugares/mapa.html", context)