from django.contrib import admin
from .models import *

admin.site.register(Lugar)
admin.site.register(Region)
admin.site.register(Provincia)
admin.site.register(Canton)
admin.site.register(Distrito)
